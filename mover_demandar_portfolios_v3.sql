SET SERVEROUTPUT ON SIZE UNLIMITED;
DECLARE
    
    l_portfolio_destino_name VARCHAR(255);
    l_portfolio_destino_id NUMBER;
    l_counter NUMBER;
        
    --Projetos
    CURSOR c1 IS
        SELECT DISTINCT r.request_id, 
               r.description,  
               pt.portfolio_id,                             
               lpe.lifecycle_id,               
               fs.financial_summary_id as id_resumo_financeiro,               
               lpe.active_entity,
               pgm.program_id               
          FROM pfm_lifecycle_parent_entity lpe
               JOIN kcrt_requests r
                 ON r.request_id = DECODE(lpe.active_entity,'Proposal',lpe.proposal_req_id,'Project',lpe.project_req_id,'Asset',lpe.asset_req_id)                    
               JOIN fm_financial_summary fs
                 ON lpe.lifecycle_id = fs.parent_id 
                    AND fs.parent_entity_id = 526 --lifecycle_entities
                    AND fs.entity_type = 'FS'
               JOIN kcrt_req_header_details rhd
                 ON rhd.request_id = r.request_id        
          LEFT JOIN pfm_portfolio_contents pc
                 ON pc.financial_summary_id = fs.financial_summary_id 
          LEFT JOIN pfm_portfolios pt
                 ON pt.portfolio_id = pc.portfolio_id                                
          LEFT JOIN pgm_program_content pgmc
                 ON pgmc.content_id = lpe.lifecycle_id
          LEFT JOIN pgm_programs pgm
                 ON pgm.program_id = pgmc.program_id
                    AND pgm.rollupable = 'Y'
          LEFT JOIN kcrt_fg_pfm_project fppj
                 ON lpe.active_entity = 'Project'
                    AND fppj.request_id = lpe.project_req_id
          LEFT JOIN kcrt_fg_pfm_proposal fpp
                 ON lpe.active_entity = 'Proposal'
                    AND fpp.request_id = lpe.project_req_id
        WHERE lpe.active_entity IN ('Proposal','Project')  
        AND rhd.parameter1 IN (
        -- Inserir códigos no formato 'código', 'código', 'código'...
        )ORDER BY r.description,pgm.program_id ASC;
                 
BEGIN

  l_counter:= 0;
  l_portfolio_destino_id := ; --Inserir código do protfólio. Caso não sáiba, entre na tabela PFM Portfolio
  select p.name into l_portfolio_destino_name from pfm_portfolios p where p.portfolio_id = l_portfolio_destino_id;
          
FOR c1_rec IN c1
    LOOP            

      --Caso o programa possua gerenciamento de portfólio ativo, a demanda não pode pertencer a um portfólio.
      --Esse bloco realiza esse teste e desassocia a demanda do portfólio
      IF c1_rec.portfolio_id IS NOT NULL THEN
        DELETE 
          FROM pfm_portfolio_contents
         WHERE portfolio_id = c1_rec.portfolio_id
               AND financial_summary_id = c1_rec.id_resumo_financeiro;        
      END IF;    

      INSERT INTO pfm_portfolio_contents(portfolio_id,financial_summary_id)
                  VALUES (l_portfolio_destino_id, c1_rec.id_resumo_financeiro);

      IF c1_rec.active_entity = 'Project' THEN
        UPDATE kcrt_fg_pfm_project
           SET prj_portfolio_id = l_portfolio_destino_id,
               prj_portfolio_name = l_portfolio_destino_name
         WHERE request_id = c1_rec.request_id;
      ELSIF c1_rec.active_entity = 'Proposal' THEN
        UPDATE kcrt_fg_pfm_proposal
           SET prop_portfolio_id = l_portfolio_destino_id,
               prop_portfolio_name = l_portfolio_destino_name
         WHERE request_id = c1_rec.request_id;
      END IF;

       
      UPDATE kcrt_requests
         SET last_update_date = sysdate,
             last_updated_by = 1
       WHERE request_id = c1_rec.request_id;
        
      UPDATE kcrt_request_details
         SET last_update_date = sysdate,
             last_updated_by = 1
       WHERE request_id = c1_rec.request_id;
       
      UPDATE kcrt_req_header_details
         SET last_update_date = sysdate,
             last_updated_by = 1
       WHERE request_id = c1_rec.request_id;                        

      dbms_output.put_line(c1_rec.request_id || ' - ' || c1_rec.description || ' - movido.');
      l_counter := l_counter + 1;
      
    END LOOP;
    dbms_output.put_line('Portfólio de Destino = ' || l_portfolio_destino_name || ' Total Movido = ' || l_counter); 
    
END;
/